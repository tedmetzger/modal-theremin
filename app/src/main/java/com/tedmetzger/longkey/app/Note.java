package com.tedmetzger.longkey.app;

/**
 * Created by jamesmetzger on 7/18/14.
 */
public class Note {
    private String name;
    double  frequency;

    public Note(String name, double frequency){
        this.name = name;
        this.frequency = frequency;
    }

    public double getFrequency() {
        return frequency;
    }

    public String getName() {
        return name;
    }
}

