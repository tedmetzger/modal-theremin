package com.tedmetzger.longkey.app;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by jamesmetzger on 7/17/14.
 */
public class Key {


    ArrayList<Note> notes = new ArrayList<Note>();
    public String rootName;
    public float rootFrequency;
    public int octave;

    private ScalePattern scalePattern;



    public Key(String rootName, String scaleName, int octave) {
        this(rootName, new ScalePattern(scaleName),octave);
    }

    public Key(String rootName, ScalePattern scalePattern, int octave){

        this.rootName = rootName;
        this.rootFrequency = Frequencies.getFrequencyForName(rootName.toLowerCase(), octave);
        this.scalePattern = scalePattern;
        this.octave = octave;


        //find the offset Position for the rootnote of the key
        int offset =0;
        for(int i=0;i< scalePattern.noteNames.length;i++){

            String lcNoteName = scalePattern.noteNames[i].toLowerCase();

            if(lcNoteName.equals(this.rootName.toLowerCase())){
                offset = i;
                break;
            }
        }

        for(int i=0;i<scalePattern.getPattern().length;i++) {

            int noteNamePosition = scalePattern.getPattern()[i];
            noteNamePosition +=offset;
            noteNamePosition = noteNamePosition% scalePattern.getNoteNamesLength();
            int noteFrequencyRatioPosition = scalePattern.getPattern()[i];
            Note n = new Note(scalePattern.getNameForPosition(noteNamePosition), scalePattern.getFrequency(rootFrequency, noteFrequencyRatioPosition));
            notes.add(n);

        }

        //add as much of another octave as will fit
        int spacesLeft = 14-notes.size();
        if(spacesLeft > 0) {
            for (int i = 0; (i < spacesLeft && i < scalePattern.getPattern().length); i++) {

                int noteNamePosition = scalePattern.getPattern()[i];
                noteNamePosition += offset;
                noteNamePosition = noteNamePosition % scalePattern.getNoteNamesLength();
                int noteFrequencyRatioPosition = scalePattern.getPattern()[i];

                Note n = new Note(scalePattern.getNameForPosition(noteNamePosition), 2 * scalePattern.getFrequency(rootFrequency, noteFrequencyRatioPosition));
                notes.add(n);

            }
        }

        spacesLeft = 14-notes.size();
        if(spacesLeft > 0) {
            for (int i = 0; (i < spacesLeft && i < scalePattern.getPattern().length); i++) {

                int noteNamePosition = scalePattern.getPattern()[i];
                noteNamePosition += offset;
                noteNamePosition = noteNamePosition % scalePattern.getNoteNamesLength();
                int noteFrequencyRatioPosition = scalePattern.getPattern()[i];

                Note n = new Note(scalePattern.getNameForPosition(noteNamePosition), 4 * scalePattern.getFrequency(rootFrequency, noteFrequencyRatioPosition));
                notes.add(n);

            }
        }

    }



    public int getOctave(){
        return octave;
    }

    public ScalePattern getScalePattern(){
        return this.scalePattern;
    }

    public Note getNoteForSliderValue(double sliderval){

        double eachSliderDivision = 1.0f/notes.size();
        int convertedSliderValue = (int)(sliderval/eachSliderDivision);
        Note note = notes.get(convertedSliderValue);
        return note;
    }

    public ArrayList<Note> getNotes(){
        return this.notes;
    }


    public static String getNextNoteName(String currentNote){
        int nextNoteIndex = -1;

        for(int i = 0;i< ScalePattern.noteNames.length;i++) {
            if(currentNote.equals(ScalePattern.noteNames[i])){
                nextNoteIndex = i+1;
                if(nextNoteIndex>= ScalePattern.noteNames.length){
                    nextNoteIndex=0;
                }
                break;
            }
        }
        return ScalePattern.noteNames[nextNoteIndex];
    }


    public static String getNextScaleName(String currentScale){
        int nextScaleIndex = -1;

        for(int i = 0;i< ScalePattern.scaleNames.length;i++) {
            if(currentScale.equals(ScalePattern.scaleNames[i])){
                nextScaleIndex = i+1;
                if(nextScaleIndex>= ScalePattern.scaleNames.length){
                    nextScaleIndex=0;
                }
                break;
            }
        }
        return ScalePattern.scaleNames[nextScaleIndex];
    }






}
