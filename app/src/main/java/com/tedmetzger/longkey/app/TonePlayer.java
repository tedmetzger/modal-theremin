package com.tedmetzger.longkey.app;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;

/**
 * Created by jamesmetzger on 7/17/14.
 */
public class TonePlayer {

    Thread t;
    int sr = 44100;
    boolean isRunning = false;
    boolean isPressingSeekbar=false;
    float volume;
    double sliderVal = 0;
    Key key;
    Note nearestNoteInKey;
    static final String waveForms[] = new String[] {"Sine","Sawtooth","Square"};
    String waveForm = waveForms[0];

    public static enum NAME {ROOT, OCTAVE_DOWN, OVERTONE_1, OVERTONE_2};
    NAME name;


    int buffsize = AudioTrack.getMinBufferSize(sr,
            AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT);
    AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
            sr, AudioFormat.CHANNEL_OUT_MONO,
            AudioFormat.ENCODING_PCM_16BIT, buffsize,
            AudioTrack.MODE_STREAM);

    public TonePlayer (NAME name, Key key){
        this(name, key, waveForms[0])  ;
    }

    public TonePlayer (NAME name, Key key, String waveForm){
        this.key = key;
        this.nearestNoteInKey = this.key.getNotes().get(0);
        this.name = name;
        this.waveForm = waveForm;
       // setVolumeToMax();
    volume = 0;
        startAudio(key);
    }

    public void setKey(Key key){
       setKey(key, waveForms[0]);
    }
    public void setKey(Key key, String waveForm){
        this.key = key;
        this.nearestNoteInKey = this.key.getNotes().get(0);
    }

    public void setWaveForm(String waveForm){
      this.waveForm = waveForm;
    }
    public NAME getName(){
        return this.name;
    }

    public void setVolumeToMax(){
        volume = name != NAME.ROOT ? audioTrack.getMaxVolume()/3: audioTrack.getMaxVolume();
    }

    public static String getNextWaveFormName(String currentWaveForm){
        int nextWaveFormIndex = -1;

        for(int i = 0;i< waveForms.length;i++) {
            if(currentWaveForm.equals(waveForms[i])){
                nextWaveFormIndex = i+1;
                if(nextWaveFormIndex>= waveForms.length){
                    nextWaveFormIndex=0;
                }
                break;
            }
        }
        return waveForms[nextWaveFormIndex];
    }
    public void setIsPressingSeekBar(boolean isPressing) {
        isPressingSeekbar = isPressing;
        if(isPressing){
            setVolumeToMax();
        }
    }

    public void setSliderVal(double sliderVal){
        this.sliderVal = sliderVal;
        nearestNoteInKey = key.getNoteForSliderValue(sliderVal);
    }

    private void startAudio(Key k) {
        final Key key = k;
        Note rootNote = key.getNotes().get(0);
        final float frequency = (float)rootNote.getFrequency();
        isRunning = true;

        // start a new thread to synthesise audio
        t = new Thread() {
            public void run() {
                // set process priority
                setPriority(Thread.MAX_PRIORITY);
                // set the buffer size

                // create an audiotrack object


                short samples[] = new short[buffsize];
                int amp = 10000;
                double twopi = 8. * Math.atan(1.);
                double fr = frequency;
                double ph = 0.0;
                int sawtoothIncrementer = 0;
                //setVolumeToMax();

                // start audio
                audioTrack.play();

                // synthesis loop
                while (isRunning) {
                    if (isPressingSeekbar) {
                        audioTrack.setStereoVolume(volume, volume);
                        setVolumeToMax();
                    } else {
                        //decay
                        float minVolume = audioTrack.getMinVolume();
                        volume = volume - (Math.abs(volume - minVolume) / 5);
                        audioTrack.setStereoVolume(volume, volume);

                    }



                    for (int i = 0; i < buffsize; i++) {



                        if(waveForm == waveForms[0]) {
                            samples[i] = (short) (amp * Math.sin(ph)); //sine wave
                        }

                        if(waveForm == waveForms[1]) {
                            samples[i] = (short) (amp * (sawtoothIncrementer % (sr / fr)) / (sr / fr)); //sawtooth wave
                        }
                        if(waveForm == waveForms[2]){
                            samples[i] = (short)(amp * Math.signum(Math.sin(ph)));//square wave
                        }

                        if (i % 100 == 0) {
                            double newFr = nearestNoteInKey.getFrequency();
                            if (fr < newFr) {
                                double difference = newFr - fr;
                                fr+= (difference/100)+1;
                            }
                            if (fr > newFr) {
                                double difference = fr - newFr;
                                fr-= (difference/100)+1;
                            }
                        }
                        ph += twopi * fr / sr;
                        sawtoothIncrementer++;
                        if(fr%sawtoothIncrementer==0)sawtoothIncrementer=0;
                    }

                    audioTrack.write(samples, 0, buffsize);


                }

                    audioTrack.stop();
                    audioTrack.release();

            }
        };
        t.start();

    }

    public void killTone(){
        isRunning = false;
        try {
            if(t!= null) {
                t.join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t = null;

    }

    public void killWithInterrupt(){

        isRunning = false;
        if(t!= null) {
            t.interrupt();
        }
        t = null;

    }

}