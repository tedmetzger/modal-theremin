package com.tedmetzger.longkey.app;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jamesmetzger on 7/29/14.
 */
public class ScalePattern {
    static final int majorKeyPattern[] = new int[] {0, 2, 4, 5, 7, 9, 11};
    static final int minorKeyPattern[] = new int[] {0, 2, 3, 5, 7, 8, 11};
    static final int majorPentatonicPattern[] = new int[] {0, 2, 4, 7, 9};
    static final int chromaticPattern[] = new int[] {0,1,2,3,4,5,6,7,8,9,10,11};
    static final int minorPentatonicPattern[] = new int[] {0,3,5,7,10};
    static final int insenPattern[] = new int[] {0,1,5,7,10};
    static final String noteNames[] = new String[] {"C","C#","D","Eb","E","F","F#","G","Ab","A","Bb","B"};
    static final String scaleNames[] = new String[] {"Major","Minor","Minor Pentatonic","Pentatonic", "Chromatic","In-Sen"};
    static final double noteFrequencyRatios[] = new double[] {1,1.059463,1.122462,1.189207 ,1.259921,1.334840,1.414214,1.498307,1.587401,1.681793,1.781797,1.887749};//equal temperament


    String scaleName;
    int[] pattern;

    public ScalePattern(String scaleName){
        this.scaleName = scaleName;
        this.pattern = getPatternFromScaleName(scaleName);
    }

    public int[] getPattern(){
        return this.pattern;
    }
    private static int[] getPatternFromScaleName(String scaleName){
        int notePattern [] = minorPentatonicPattern;

        if(scaleName.equals(ScalePattern.scaleNames[0])){
            notePattern = majorKeyPattern;
        }

        if(scaleName.equals(ScalePattern.scaleNames[1])){
            notePattern = minorKeyPattern;
        }

        if(scaleName.equals(ScalePattern.scaleNames[2])){
            notePattern = minorPentatonicPattern;
        }

        if(scaleName.equals(ScalePattern.scaleNames[3])){
            notePattern = majorPentatonicPattern;
        }

        if(scaleName.equals(ScalePattern.scaleNames[4])){
            notePattern = chromaticPattern;
        }

        if(scaleName.equals(ScalePattern.scaleNames[5])){
            notePattern = insenPattern;
        }

        return notePattern;

    }

    public String getNameForPosition(int position){
       return noteNames[position];
    }

    public double getFrequency(float rootFrequency, int position){
        return rootFrequency* noteFrequencyRatios[position];
    }

    public static int getNoteNamesLength(){
        return noteNames.length;
    }


}
