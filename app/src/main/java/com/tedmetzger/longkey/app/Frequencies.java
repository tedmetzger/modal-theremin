package com.tedmetzger.longkey.app;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jamesmetzger on 7/29/14.
 */



public class Frequencies {

    static final int DEFAULT_OCTAVE = 4;
    static final int OCTAVE_DOWN = 3;
    static final int OCTAVE_UP = 5;

    static Map<String,Float> rootFrequencies = new HashMap<String, Float>();

    static {


        rootFrequencies.put("c",32.703f);
        rootFrequencies.put("c#",34.648f);
        rootFrequencies.put("d",36.708f);
        rootFrequencies.put("eb",38.891f);
        rootFrequencies.put("e",41.203f);
        rootFrequencies.put("f",43.654f);
        rootFrequencies.put("f#",46.255f);
        rootFrequencies.put("g",48.999f);
        rootFrequencies.put("ab",51.91f);
        rootFrequencies.put("a",55.000f);
        rootFrequencies.put("bb",58.27f);
        rootFrequencies.put("b",61.735f);


    }

    public static float getFrequencyForName(String name) {
        return getFrequencyForName(name, DEFAULT_OCTAVE);
    }

    public static float getFrequencyForName(String name, int octave) {
        if(octave < 1) octave =1;
        if(octave > 6) octave = 6;
        double freq  = Math.pow(2,(octave-1)) * rootFrequencies.get(name);
        return (float)freq;
    }
}
