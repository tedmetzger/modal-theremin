package com.tedmetzger.longkey.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends Activity {


    SeekBar fSlider;
    double sliderval;
    private ArrayList<TonePlayer> tonePlayers = new ArrayList<TonePlayer>();
    private LinearLayout noteMarkers;
    Button rootButton, overtoneButton1, overtoneButton2, getOvertoneButton2WaveForm, scaleButton;
    static final int TONE_PLAYERS_SIZE = 4; // root, octave, overtone_1, overtone_2
    static final String defaultRoot = ScalePattern.noteNames[0];//C
    static final String defaultOvertone2 = ScalePattern.noteNames[0];//C
    static final String defaultScale = ScalePattern.scaleNames[3];//Pentatonic
    static final String defaultOvertone2WaveForm = TonePlayer.waveForms[0]; // sine
    String currentOvertone1 = null;
    String currentOvertone2 = null;
    Key currentKey;

    boolean isStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fSlider = (SeekBar) findViewById(R.id.fSlider);
        noteMarkers = (LinearLayout)findViewById(R.id.noteMarkers);
        currentKey = new Key(defaultRoot, defaultScale, Frequencies.DEFAULT_OCTAVE);


        // create a listener for the slider bar;
        SeekBar.OnSeekBarChangeListener listener = new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
                setTonePlayerIsPressingSeekBar(false);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                setTonePlayerIsPressingSeekBar(true);
            }

            public void onProgressChanged(SeekBar seekBar,
                                          int progress,
                                          boolean fromUser) {
                if(fromUser) sliderval = progress / (double)seekBar.getMax();
                setTonePlayerSliderBarVal(sliderval);
                setTonePlayerIsPressingSeekBar(true);
            }
        };

        // set the listener on the slider
        fSlider.setOnSeekBarChangeListener(listener);
        rootButton = (Button)findViewById(R.id.root);
        overtoneButton1 = (Button)findViewById(R.id.overtone1);
        overtoneButton2 = (Button)findViewById(R.id.overtone2);
        getOvertoneButton2WaveForm = (Button)findViewById(R.id.overtone2Waveform);
        scaleButton = (Button)findViewById(R.id.scale);

        setRoot(defaultRoot);
        setScale(defaultScale);
        setOvertone2WaveForm(defaultOvertone2WaveForm);


        rootButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleRootButtonClick(view);
            }
        });

        overtoneButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOvertone1ButtonClick(view);
            }
        });

        overtoneButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOvertone2ButtonClick(view);
            }
        });

        getOvertoneButton2WaveForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOvertone2WaveFormButtonClick(view);
            }
        });

        scaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleScaleButtonClick(view);
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
        updateForKey(currentKey);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        for(TonePlayer tp :tonePlayers){
            tp.killWithInterrupt();
        }
        tonePlayers.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
        for(TonePlayer tp :tonePlayers){
            tp.killWithInterrupt();
        }
        tonePlayers.clear();
    }



    private void setTonePlayerIsPressingSeekBar(boolean isPressing){
        for(TonePlayer tp:tonePlayers){
            tp.setIsPressingSeekBar(isPressing);
        }
    }

    private void setTonePlayerSliderBarVal(double sliderVal){
        for(TonePlayer tp:tonePlayers){
            tp.setSliderVal(sliderVal);
        }
    }


    private void updateForKey(Key k){

        isStarted = true;
        Key overtone1 = new Key(currentOvertone1, k.getScalePattern(), Frequencies.DEFAULT_OCTAVE);
        Key overtone2 = new Key(currentOvertone2, k.getScalePattern(), Frequencies.OCTAVE_UP);
        Key octaveDown = new Key(k.getNotes().get(0).getName(),k.getScalePattern(), Frequencies.OCTAVE_DOWN);

        if(tonePlayers.size() != TONE_PLAYERS_SIZE){
            for(TonePlayer tp :tonePlayers){
                tp.killWithInterrupt();
            }
            tonePlayers.clear();
            tonePlayers.add(new TonePlayer(TonePlayer.NAME.ROOT,k));
            tonePlayers.add(new TonePlayer(TonePlayer.NAME.OVERTONE_1,overtone1));
            tonePlayers.add(new TonePlayer(TonePlayer.NAME.OVERTONE_2,overtone2));
            tonePlayers.add(new TonePlayer(TonePlayer.NAME.OCTAVE_DOWN,octaveDown));

        } else{
            for(TonePlayer tp : tonePlayers){
                if(tp.getName().equals(TonePlayer.NAME.ROOT)) tp.setKey(k);
                if(tp.getName().equals(TonePlayer.NAME.OVERTONE_1)) tp.setKey(overtone1);
                if(tp.getName().equals(TonePlayer.NAME.OVERTONE_2)) tp.setKey(overtone2);
                if(tp.getName().equals(TonePlayer.NAME.OCTAVE_DOWN)) tp.setKey(octaveDown);
            }

        }
        updateViewsForKey(k);


    }

    public void updateViewsForKey(Key k){
        noteMarkers.removeAllViews();
        ArrayList<Note> notes = k.getNotes();

        for(int i = 0;i<notes.size();i++){

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1);
            TextView v = new TextView(this);
            v.setGravity(Gravity.CENTER);
            if(i%2==0) {
                v.setBackgroundColor(0xFF653684);//purple
                v.setTextColor(0xFF6CBE1C);

            }else{
                v.setBackgroundColor(0xFF6CBE1C);//green
                v.setTextColor(0xFF653684);
            }
            String noteName = notes.get(i).getName();
            v.setText(String.valueOf(noteName));
            noteMarkers.addView(v,params);

        }
    }


    private void handleRootButtonClick(View v){
        String nextNoteName;
        Button b = (Button)v;
        String buttonText = b.getText().toString();

        if(buttonText==null){
            nextNoteName = ScalePattern.noteNames[0];
        } else {
            nextNoteName = Key.getNextNoteName(buttonText);
        }
        setRoot(nextNoteName);


    }

    private void handleOvertone1ButtonClick(View v){

        advanceOvertone1();
    }

    private void handleOvertone2ButtonClick(View v){

        advanceOvertone2();
    }

    private void handleOvertone2WaveFormButtonClick(View v){
        String nextWaveFormName;
        Button b = (Button)v;
        String buttonText = b.getText().toString();

        if(buttonText==null){
            nextWaveFormName = TonePlayer.waveForms[0];
        } else {
            nextWaveFormName = TonePlayer.getNextWaveFormName(buttonText);
        }
        setOvertone2WaveForm(nextWaveFormName);

    }

    private void handleScaleButtonClick(View v){
        String nextScaleName;
        Button b = (Button)v;
        String buttonText = b.getText().toString();

        if(buttonText==null){
            nextScaleName = ScalePattern.scaleNames[0];
        } else {
            nextScaleName = Key.getNextScaleName(buttonText);
        }
        setScale(nextScaleName);

    }

    public void setRoot(String rootNote){
        rootButton.setText(rootNote);
        advanceOvertone1();//when the user advances the key, advance the overtone as well
        advanceOvertone2();//when the user advances the key, advance the overtone as well
    }

    public void advanceOvertone1(){
        currentOvertone1 = currentOvertone1 == null ? defaultRoot :  Key.getNextNoteName(currentOvertone1);//when the user advances the key, advance the overtone as well
        Key key = new Key(rootButton.getText().toString(), scaleButton.getText().toString(), Frequencies.DEFAULT_OCTAVE);
        overtoneButton1.setText(currentOvertone1);
        if(isStarted) {
            updateForKey(key);
        }

    }

    public void advanceOvertone2(){
        currentOvertone2 = currentOvertone2 == null ? defaultOvertone2 :  Key.getNextNoteName(currentOvertone2);//when the user advances the key, advance the overtone as well
        Key key = new Key(rootButton.getText().toString(), scaleButton.getText().toString(), Frequencies.OCTAVE_UP);
        overtoneButton2.setText(currentOvertone2);
        if(isStarted) {
            updateForKey(key);
        }

    }

    public void setScale(String scale){
        Key key = new Key(rootButton.getText().toString(), scale, Frequencies.DEFAULT_OCTAVE);
        if(isStarted) {
            updateForKey(key);
        }
        scaleButton.setText(scale);
    }

    public void setOvertone2WaveForm(String waveForm){
        for(TonePlayer tp : tonePlayers){
            if(tp.getName().equals(TonePlayer.NAME.OVERTONE_2)) tp.setWaveForm(waveForm);
        }
        getOvertoneButton2WaveForm.setText(waveForm);
    }



}
